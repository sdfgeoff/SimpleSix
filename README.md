Simple Six is an online multiplayer game powered by Playcanvas, ammo.js and
some wizardry I cooked up.

It aims to be a fusion of two dos games:
 - Descent (the pinacle of 6DOF games)
 - Splay (a best-of-three-kills 2D space shooter)
 
So it's a 6DOF space-ship combat game but with only homing missiles and lasers.

You can play it at:
www.simplesix.sdfgeoff.space
