import socket, hashlib, base64, threading
import json
import Queue
import struct
import time

def log(msg):
	log_str = time.strftime("%y/%m/%d %H:%M:%S ||",time.localtime()) + msg
	print log_str
	#with open("log.txt", "a") as myfile:
	#	myfile.write(log_str+'\n')


class GameList:
	gl = list()
	def __init__(self):
		pass
		
	def add_game(self, client):
		if client not in self.gl:
			self.gl.append(client)

	
	def end_game(self, client):
		while client in self.gl:
			self.gl.remove(client)
	
	def get_list(self):
		gd = {'games':[c.game for c in self.gl]}
		return(json.dumps(gd))

class Client:
	conn = None
	addr = None
	from_server = None
	to_server = None
	game = None
	def __init__(self, server, conn, addr, from_server, to_server, idnum):
		self.conn = conn
		self.addr = addr
		self.from_server = from_server
		self.to_server = to_server
		self.idnum = idnum
		self.server = server
		
	def threaded_loop(self):
		'''INSIDE THIS FUNCTION ONLY EVER ACCESS THE TO AND FROM 
		SERVER QUEUES AND THE SEND AND RECIEVE FUNCTIONS'''
		self.conn.settimeout(1)
		while 1:         
			data = self.recv_data(self.conn)
			if data != None:
				log("From "+str(self.idnum)+": "+data)
				#log("received [%s]" % (data,))
				if data == 'LIST':
					self.to_server.put('LIST')
				elif data[0:4] == 'HOST':
					self.to_server.put(data)
				elif data[0:4] == 'MSSG':
					self.to_server.put(data)
				else:
					self.send('ERRO{"type":"error", "data":"Message not understood"}')
			if not self.from_server.empty():
				snd = self.from_server.get()
				self.send(snd)
				log("To   "+str(self.idnum)+": "+snd)
	
	def non_threaded_step(self, game_list):
		if not self.to_server.empty():
			rec = self.to_server.get()
			if rec == 'LIST':
				self.from_server.put('LIST'+game_list.get_list())
			elif rec[0:4] == 'HOST':
				self.game = json.loads(rec[4:])
				self.game['id'] = self.idnum
				game_list.add_game(self)
			elif rec[0:4] == 'MSSG':
				try:
					d = json.loads(rec[4:])
					to = [c for c in self.server.active_clients if c.idnum == d['id']][0]
					to.from_server.put("MSSG"+json.dumps({'data':str(d['data']), 'id':self.idnum}))
				except:
					self.from_server.put('ERRO{"type":"error", "data":"Client does not exist"}')
					
				

	def send(self, data):

		if len(data) < 2**7 - 3: #If 125 or less
			resp = bytearray([0b10000001, len(data)])
		elif len(data) < 2**16: #If the packet will fit into the second length bracket
			resp = bytearray([0b10000001, 126]) + struct.pack(">H",len(data)) 
		elif len(data) < 2**64: #If the packet fits within the websocket limits
			resp = bytearray([0b10000001, 127]) + struct.pack(">Q",len(data)) 
			
		for d in bytearray(data):
			resp.append(d)
		self.conn.send(resp)

	def recv_data (self, client):
		try:
			data = bytearray(client.recv(2048))
		except socket.timeout:
			return None
		if(len(data) < 6):
			raise Exception("Error reading data")
		assert(0x1 == (0xFF & data[0]) >> 7)
		assert(0x1 == (0xF & data[0]))
		assert(0x1 == (0xFF & data[1]) >> 7)
		datalen = (0x7F & data[1])
		start_pos = 0
		if datalen == 126:
			#Need to read next section for actual length
			datalen = (data[2] << 8) + data[3]
			#datalen = #Assemble data[2] and data[3] into a number
			start_pos += 2
		elif datalen == 127:
			print("Data too long and I was lazy when programming this")
		
		str_data = ''
		if(datalen > 0):
			mask_key = data[2+start_pos:6+start_pos]
			masked_data = data[6+start_pos:(6+start_pos+datalen)]
			unmasked_data = [masked_data[i] ^ mask_key[i%4] for i in range(len(masked_data))]
			str_data = str(bytearray(unmasked_data))
		return str_data

class PyWSock:
	MAGIC = '258EAFA5-E914-47DA-95CA-C5AB0DC85B11'
	HSHAKE_RESP = "HTTP/1.1 101 Switching Protocols\r\n" + \
				"Upgrade: websocket\r\n" + \
				"Connection: Upgrade\r\n" + \
				"Sec-WebSocket-Accept: %s\r\n" + \
				"\r\n"

	active_clients = list()
	active_games = GameList()
	
	idnum = 0

	def parse_headers (self, data):
		headers = {}
		lines = data.splitlines()
		for l in lines:
			parts = l.split(": ", 1)
			if len(parts) == 2:
				headers[parts[0]] = parts[1]
		headers['code'] = lines[len(lines) - 1]
		return headers

	def handshake (self, client):
		data = client.recv(2048)
		headers = self.parse_headers(data)

		key = headers['Sec-WebSocket-Key']
		resp_data = self.HSHAKE_RESP % ((base64.b64encode(hashlib.sha1(key+self.MAGIC).digest()),))
		#print('Response: [%s]' % (resp_data,))
		return client.send(resp_data)

	def handle_client (self, client):
		self.handshake(client.conn)
		log("New ID: "+str(client.idnum) + " is " + str(client.addr[0]))
		try:
			client.threaded_loop()
		except Exception as e:
			if str(e) != '':
				log("Exception %s" % (str(e)))
		log('End ID: ' + str(client.idnum))
		self.active_clients.remove(client)
		client.conn.close()
		self.active_games.end_game(client)

	def start_server (self, port):
		
		s = socket.socket()
		s.settimeout(1)
		s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		s.bind(('', port))
		s.listen(5)
		while(1):
			try:
				conn, addr = s.accept()
				#log ('Connection from: ' + str(addr))
				
				to_server = Queue.Queue()
				from_server = Queue.Queue()
				c = Client(self, conn, addr, from_server, to_server, self.idnum)
				self.idnum += 1
				
				threading.Thread(target = self.handle_client, args = [c]).start()
				self.active_clients.append(c)
			
			except socket.timeout:
				pass
			#Check for clients reporting active games and asking for tracker information
			for c in self.active_clients:
				c.non_threaded_step(self.active_games)
