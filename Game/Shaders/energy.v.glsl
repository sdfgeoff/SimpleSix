        attribute vec3 aNormal;
        attribute vec2 aUv0;
        attribute vec3 aPosition;
        
        uniform mat4   matrix_viewProjection;
        uniform mat4   matrix_model;
        uniform mat4   matrix_view;
        uniform mat3   matrix_normal;


        varying vec2 to_frag_UV;
        varying float to_frag_grad;

        void main(void)
        {
            
            to_frag_UV = aUv0;
            
            vec3 eyeNormal = normalize(matrix_normal * aNormal);
            to_frag_UV.x += eyeNormal.x;
            to_frag_UV.y += eyeNormal.y;

            vec3 viewvec = vec3(0.0,0.0,0.0);
            viewvec.x = matrix_view[0][2];
            viewvec.y = matrix_view[1][2];
            viewvec.z = matrix_view[2][2];
            //viewvec = matrix_view * aPosition;

            to_frag_grad = max(0.0, dot(eyeNormal, viewvec));
            

            vec4 pos = matrix_model * vec4(aPosition, 1.0);
            gl_Position = matrix_viewProjection * pos;
        }
