requirejs('config.js'); //For the keymap
requirejs('common.js'); //For the clamp function
requirejs('task.js'); //To update the control every frame

mousedelta = new pc.Vec2();
controldict = {};

KEYS_NUMS = {}

CONSTANTS = {
            'chat':'8',
            'help':'F1',
            'submit':'ENTER',
        }

function initControl(){
    loadconfig();
    mouse.on(pc.EVENT_MOUSEMOVE, mouseMove);
    mouse.on(pc.EVENT_MOUSEDOWN, mouseClick);
    mouse.disableContextMenu();
    
    //Turn key names into numbers
    for (key in KEYS){
        KEYS_NUMS[key] = pc['KEY_'+KEYS[key]];
    }
    for (con in CONSTANTS){
        KEYS_NUMS[con] = pc['KEY_'+CONSTANTS[con]];
    }
}

function mouseMove(event){
    mx = event.dx;
    my = event.dy;
    if (Math.abs(mx) <= 1){ //Two pixel deadzone. For some reason one pixel doesn't work
        mx = 0;
    } 
    if (Math.abs(my) <= 1){
        my = 0;
    }
    mousedelta.x = clamp(mx/app.graphicsDevice.height*MOUSE['XSensitivity'], -1, 1);
    mousedelta.y = clamp(-my/app.graphicsDevice.height*MOUSE['YSensitivity'], -1, 1);
}

function mouseClick (event) {
    // When the mouse button is clicked try and capture the pointer
    if (!pc.Mouse.isPointerLocked()) {
        mouse.enablePointerLock();
    }
}

function getKey(key){
    return keyboard.isPressed(KEYS_NUMS[key]);
}


function controlUpdate(dt){
    //Updates the player control
    //console.log(keyboard.isPressed(pc['KEY_'+KEYS['SForwards']]) == 1)
    mouse.update()
    controldict['Sf'] =  getKey('SForwards') - getKey('SBackwards')
    controldict['Ss'] =  getKey('SLeft') - getKey('SRight')
    controldict['Su'] =  getKey('SUp') - getKey('SDown')
    
    controldict['Roll'] =  getKey('RollLeft') - getKey('RollRight')
    controldict['Em'] = getKey('EmergencyThrust')
    
    //Mouse:
    controldict['Sec'] = mouse.isPressed(pc['MOUSEBUTTON_RIGHT'])
    controldict['Pri'] = mouse.isPressed(pc['MOUSEBUTTON_LEFT'])
    controldict['Mid'] = mouse.isPressed(pc['MOUSEBUTTON_MIDDLE'])
    
    controldict['Tilt'] = mousedelta.y
    controldict['Pan'] = -mousedelta.x 
    mousedelta.scale(0)
    
    playerObj.updateControl(dt, controldict)
    
    //Game state controls:
    if (keyboard.isPressed(pc['KEY_PRINT_SCREEN'])){
        window.open(canvas.toDataURL());
    }
    
    
}

continueloading = true
