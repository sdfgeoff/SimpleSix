requirejs('ship.js')

function initDrone(ship){
    //Turns a ship into a drone
    
    col = hslToRgb(DRONE['color']/360, 1, 0.5)
    for (me in ship.obj.model.model.meshInstances){
		matName = ship.obj.model.model.meshInstances[me].material.name
		if (matName == 'PlayerColor.json' || matName == 'Reticle.json'){
			mat = ship.obj.model.model.meshInstances[me].material.clone()
			ship.obj.model.model.meshInstances[me].material = mat
			mat.diffuse = new pc.Color(col[0], col[1], col[2])
			mat.update()
		}
	}
}

function dieDrone(d, pos, powerups){
    pos = listToVec(pos)
    placeExplosion(pos)
    for (p in powerups){
        makePowerup(pos, listToVec(powerups[p]))
    }
    
    d.obj.destroy()
    droneObj = null
    
}

function spawnDrone(pos){
    //Starts spawn animation etc.
    //Returns the new drone
    d = new ship();//
    d.init(pos, false);

    initDrone(d);
    
    return d;   
}

continueloading=true
