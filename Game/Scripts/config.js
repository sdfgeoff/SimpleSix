requirejs('cookie.js')

KEY_LIST = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'TAB', 'RETURN', 'ENTER', 'SHIFT', 'CONTROL', 'ALT', 'CAPS_LOCK', 'SPACE', 'PAGE_UP', 'PAGE_DOWN', 'END', 'HOME', 'LEFT', 'UP', 'RIGHT', 'DOWN', 'INSERT', 'DELETE', 'SEMICOLON', 'EQUAL', 'WINDOWS', 'NUMPAD_0', 'NUMPAD_1', 'NUMPAD_2', 'NUMPAD_3', 'NUMPAD_4', 'NUMPAD_5', 'NUMPAD_6', 'NUMPAD_7', 'NUMPAD_8', 'NUMPAD_9', 'MULTIPLY', 'ADD', 'SEPARATOR', 'SUBTRACT', 'DECIMAL', 'DIVIDE', 'COMMA', 'PERIOD', 'SLASH', 'OPEN_BRACKET', 'BACK_SLASH', 'CLOSE_BRACKET', 'META']
KEYS = {	'SForwards': 'W',
			'SBackwards': 'S', 
			'SLeft': 'A', 
			'SRight': 'D',
			'SUp': 'SPACE',
			'SDown': 'SHIFT',
			
			'RollLeft': 'Q',
			'RollRight': 'E',
			'EmergencyThrust':'V',
		}
		
MOUSE = {	'XSensitivity':30,
			'YSensitivity':-20,
		}

PILOT = {
			'pilotname':'DefaultPilot',
			'color':180,
		}
        


function loadconfig(){
	dict = JSON.parse(readCookie('SimpleSix'))
	if (dict != null){
		KEYS = dict['keys']
		MOUSE = dict['mouse']
		PILOT = dict['pilot']
	}
	return {'keys':KEYS, 'mouse':MOUSE, 'pilot':PILOT}
}

function saveconfig(){
	
	for (control in KEYS){
		KEYS[control] = document.getElementById(control).value
	}
	for (m in MOUSE){
		MOUSE[m] = document.getElementById(m).value
	}
	for (p in PILOT){
		PILOT[p] = document.getElementById(p).value
	}
	dict = {'keys':KEYS, 'mouse':MOUSE, 'pilot':PILOT}
	createCookie('SimpleSix',JSON.stringify(dict),365)
}

function resetconfig(){
	eraseCookie('SimpleSix')
}

function getPData(){
    loadconfig()
    return PILOT
}

continueloading=true
