requireobj('Explosion1');
requireobj('Explosion2');
requireobj('Explosion3');
requirejs('player.js');
requirejs('common.js');

requiresound('Explode')

MISSILE_DAMAGE = 30
MISSILE_FORCE = 5

function explosion(){
    //Class for an explosion
}

explosion.prototype.collide = function(result){

}


explosion.prototype.init = function(pos){
    this.obj1 = addExplosionElement('Explosion1.json', pos, 0.1)
    this.obj2 = addExplosionElement('Explosion2.json', pos, 0.1)
    this.obj3 = addExplosionElement('Explosion3.json', pos, 0.1)
    this.time = 0;
    this.color = new pc.Color()

    this.mat = this.obj1.model.model.meshInstances[0].material.clone()
    this.obj1.model.model.meshInstances[0].material = this.mat
    this.obj2.model.model.meshInstances[0].material = this.mat
    this.obj3.model.model.meshInstances[0].material = this.mat

    if (playerObj != null){
        vec = getVectBetween(this.obj1, playerObj.obj)
        dist = vec.lengthSq()
        dam = Math.min(MISSILE_DAMAGE/ dist / dist, MISSILE_DAMAGE)
        forc = vec.normalize().scale(-dam*MISSILE_FORCE)

        playerObj.applyForce(forc)
        playerObj.addHealth(-dam)

        //Sound:
        this.obj1.addComponent("audiosource", {
            assets: [getObj('Explode'+SOUND_FORMAT)],
            volume: 5,
        });
    }

}

explosion.prototype.animate = function(dt, e){
    if (e.obj1.model != undefined)
        e.time += dt
        es = Math.sqrt(e.time*20);
        e.obj1.setLocalScale(es,es,es);
        e.obj2.setLocalScale(es,es,es);
        e.obj3.setLocalScale(es*2,es*2,es*2);

        ec = 1 - e.time*2;
        e.color.set(ec, ec, ec, ec)

        if (e.obj1.model == undefined){
        } else {
            e.obj1.model.model.meshInstances[0].material.emissive = e.color;
            e.obj1.model.model.meshInstances[0].material.update()
        }

}


explosion.prototype.remove = function(dt, e){
    e.obj1.destroy();
    e.obj2.destroy();
    e.obj3.destroy();
}



function addExplosionElement(name, pos, scale){
    lobj = new pc.Entity();
    app.systems.model.addComponent(lobj, {
        type: "asset",
        asset: getObj(name, "model")
    });
    lobj.translate(pos);
    lobj.rotate(getRandomVect(180));
    lobj.addComponent('collision', {type:'sphere', radius:0.01});
    lobj.addComponent('rigidbody', {type:'kinematic', mass:1.0, friction:0, enabled:true});
    lobj.rigidbody.syncEntityToBody();
    app.root.addChild(lobj);
    lobj.setLocalScale(scale, scale, scale)

    lobj.rigidbody.angularVelocity = getRandomVect(100);

    return lobj;
}

function placeExplosion(pos){
    //Spawns a laser at the specified position vector
    e = new explosion();
    e.init(pos);
    addTask(e.animate, 0, 30, e);
    addTask(e.remove, 2, 1, e);
    return e

}

continueloading=true;
