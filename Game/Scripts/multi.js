PACKET_RATE = 1/20 //20pps

cfg = {"iceServers":[{"url":"stun:stun4.l.google.com:19302"},
                      {"url":"stun:stun.3cx.com"}
                      ]}
con = { 'optional': [{'DtlsSrtpKeyAgreement': true}] }


if (typeof RTCPeerConnection === 'undefined'){
    if (typeof webkitRTCPeerConnection != 'undefined'){
        RTCPeerConnection = webkitRTCPeerConnection
    } else if (typeof mozRTCPeerConnection != 'undefined'){
        RTCPeerConnection = mozRTCPeerConnection
        RTCSessionDescription = mozRTCSessionDescription
        RTCIceCandidate = mozRTCIceCandidate
    }
}

peerConnect = new RTCPeerConnection(cfg, con)
unreliableSendChannel = null
reliableSendChannel = null

reliableRecieveChannel = null
unreliableReceiveChannel = null

multiActive = false //False when other person not joined yet....
multiTask = null

var droneObj = null
//////////////////////////// IN-GAME CODE ///////////////////////////
function initMulti(){
    //Assumes active channels already, just adds to execution queue
    multiTask = addTask(updateMulti, PACKET_RATE, updateMulti)
}

function updateMulti(dt){
    if (multiActive == true){
        multistate = 'Playing'
        if (playerObj != null){
            sendUntracked('m'+JSON.stringify(getMData()))
        }
    } else {
        multistate = 'Join'
    }
}


function sendPrimaryShootEvent(){
    resetTaskTime(multiTask)
    sendUntracked('l'+JSON.stringify(getMData()))
}

function sendSecondaryShootEvent(){
    resetTaskTime(multiTask)
    sendUntracked('s'+JSON.stringify(getMData()))
}

function sendHitEvent(health){
    resetTaskTime(multiTask)
    sendUntracked('h'+JSON.stringify(getMData()))
}

function sendDeathEvent(numDeaths, pos, powerups){
    resetTaskTime(multiTask)
    sendTracked(JSON.stringify({'type':'d', 'num':numDeaths, 'p':powerups, 'pos':pos}))
}

function sendChatMessage(msg){
    sendTracked(JSON.stringify({'type':'c', 'msg':msg}))
}

function sendUntracked(msg){
    if (multiActive == true){
        unreliableSendChannel.send(msg)
    }
    
}

function sendTracked(msg){
    if (multiActive == true){
        reliableSendChannel.send(msg)
    }
}

function unreliableParse(data){
    data_type = data[0]
    data = JSON.parse(data.slice(1))
    //Movement data
    if (droneObj != null){
        pos = listToVec(data['p'])
        rot = listToVec(data['r'])
        linvel = listToVec(data['v'])
        angvel = listToVec(data['a'])
        emthrust = data['e']
    
        droneObj.obj.rigidbody.teleport(pos, rot)
        droneObj.obj.rigidbody.linearVelocity = linvel
        droneObj.obj.rigidbody.angularVelocity = angvel
        droneObj.manageThrustSound(emthrust)
        
        if (data_type == 'l'){
            droneObj.shootPrimary()
        }
        if (data_type == 's'){
            droneObj.shootSecondary()
        }
    } else {
        //Spawn a new drone!
        if (gameState != initstate){
            droneObj = spawnDrone(getSpawnPoint())
        }
    }
}

function reliableParse(data){
    d = JSON.parse(data)
    if (d['type'] == 'd'){
        gameLog(DRONE['pilotname'] + ' has died')
        player_kills = d['num']
        dieDrone(droneObj, d['pos'], d['p'])
    } else if (d['type'] == 'c'){
        gameLog(DRONE['pilotname']+': '+ c['msg'])
    } else if (d['type'] == 'level'){
        level_name = d['data']
        requireobj(level_name, 'Levels/'+level_name)
        startJoinerGame()
    } else if (d['type'] == 'pdata'){
        DRONE = d['data']
        //We now have the information required to start the game
        multiActive = true
    }
    console.log(data);
}


function disconnect(){
    gameLog(DRONE['pilotname']+' Has Disconnected')
    multiActive = false
}

////////////////////////   CREATING CHANNEL CODE //////////////////
function createDataChannels(){
    unreliableSendChannel = peerConnect.createDataChannel('unreliableSendChannel', {maxRetransmits:0});
    unreliableSendChannel.onopen = function (e) {
        console.log('unreliableSendChannel connect');
    }
    unreliableSendChannel.onerror = function (e) {
        console.log('unreliableSendChannel error', e)
        disconnect()
    }
    unreliableSendChannel.onclose = function () {
        console.log("unreliableSendChannel closed");
        disconnect()
    };
    
    
    reliableSendChannel = peerConnect.createDataChannel('reliableSendChannel');
    reliableSendChannel.onopen = function (e) {
        console.log('reliableSendChannel connect');
        onReliableConnectionEstablished()
    }
    reliableSendChannel.onerror = function (e) {
        console.log('reliableSendChannel error', e)
        disconnect()
    }
    reliableSendChannel.onclose = function () {
        console.log("reliableSendChannel closed");
        disconnect()
    };
    
    //Set callbacks:
    peerConnect.ondatachannel = function (e) {
        if (e.channel.label == 'reliableSendChannel'){
            reliableRecieveChannel = e.channel
            reliableRecieveChannel.onmessage = function(e){
                reliableParse(e.data)
            }
        } else {
            unreliableRecieveChannel = e.channel
            unreliableRecieveChannel.onmessage = function(e){
                unreliableParse(e.data)
            }
        }
    };
}


function createLocalOffer(runOnOffer) {
    //Create datachannel
    createDataChannels()
    
    //Create and return offer
    peerConnect.createOffer(function (desc) {
        peerConnect.setLocalDescription(desc, function () {}, function () {});
        runOnOffer(desc)
    }, function () {console.warn("Couldn't create offer");});
}

function parseRemoteOffer(offer, runOnAnswer, sendCandidate){
    //Create datachannel
    createDataChannels()
    //Set Callbacks:
    peerConnect.onicecandidate = function (e) {sendCandidate(e)};

    //Create and return answer:
    offerDesc = new RTCSessionDescription(offer);
    peerConnect.setRemoteDescription(offerDesc);
    peerConnect.createAnswer(function (answerDesc) {
        runOnAnswer(answerDesc)
        peerConnect.setLocalDescription(answerDesc);
    }, function () { console.warn("No create answer"); });
}

function parseRemoteAnswer(answer){
	answerDesc = new RTCSessionDescription(answer);
	peerConnect.setRemoteDescription(answerDesc);
} 

function addJoinerCandidate(candidate){
    if (candidate != null)
        peerConnect.addIceCandidate(new RTCIceCandidate(candidate))
}

function onReliableConnectionEstablished(){
    //This should run when the browsers are talking to each other. It will transfer pilotData using the reliableSendChannel
    //If this is the hoster, it should also send the levelname 
    console.log('Sending levelname and drone information')
    if (typeof(level_name) != "undefined"){
        reliableSendChannel.send(JSON.stringify({'type':'level', 'data':level_name}))
    }
    reliableSendChannel.send(JSON.stringify({'type':'pdata','data':getPData()}))
}

function startJoinerGame(){
    requirejs('game.js')
    //Stop the tracker
    console.log('Starting Game')
    tracker_msg_box = console.log
}

continueloading = true
