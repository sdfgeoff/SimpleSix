requirejs('common.js');
requirejs('task.js');
requirejs('game.js');

requireobj('Laser');
requireobj('Splash');


LASER_ENERGY = 0.3;
LASER_SPEED = 12;
LASER_FIRE_TIME = 0.3; //Seconds
LASER_DAMAGE = 8

function shootLaser(pos, rot, pos_offset, player){
    //Spawns a laser at the specified position vector
    //Player variable indicates if the laser was shot by the player
    l = new laser();
    l.init(pos, rot, pos_offset, player);
    return l
}

//// These functions/classes should not be called/instanced directly ////

function laser(){
    //Class for a laser bolt
}

laser.prototype.collide = function(result){
    contact = result.contacts[0];
    addLaserSplash(this.entity.getPosition(), contact.normal);//contact.point, contact.normal);
    this.entity.destroy();
}

laser.prototype.init = function(pos, rot, pos_offset, player){
    lobj = new pc.Entity();
    lobj.name = "Laser"
    app.systems.model.addComponent(lobj, {
        type: "asset",
        asset: getObj('Laser.json', "model")
    });
    lobj.translate(pos);
    lobj.rotate(rot);
    lobj.translateLocal(pos_offset[0], pos_offset[1], pos_offset[2]);
    lobj.addComponent('collision', {type:'capsule',
                                    radius:0.08,
                                    height:0.6,
                                    axis:2});
    if (player == true){ //Shot by player: collide with Drones

        lobj.addComponent('rigidbody', {type:'dynamic',
                                        mass:0.2,
                                        group: PLAYER_WEAP_GROUP,
                                        mask: (pc.BODYGROUP_STATIC | DRONE_GROUP)
                                        });
    } else { //Shot by drone: collide with player
        lobj.addComponent('rigidbody', {type:'dynamic',
                                        mass:0.2,
                                        group: DRONE_WEAP_GROUP,
                                        mask: (pc.BODYGROUP_STATIC | PLAYER_GROUP)
                                        });
    }
    lobj.rigidbody.syncEntityToBody();
    app.root.addChild(lobj);

    lobj.rigidbody.linearVelocity = loc2glob(new pc.Vec3(0,-LASER_SPEED,0), lobj);
    lobj.rigidbody.angularVelocity = loc2glob(new pc.Vec3(0,200,0), lobj);
    lobj.collision.on('collisionstart', this.collide);

    this.obj = lobj;
}

function laserSplash(){
    //Class for a little bit of splash!
    life = 0;
}

laserSplash.prototype.shrink = function(dt, obj){
    //console.log(obj.obj.getLocalScale().scale(0.5));
    obj.obj.setLocalScale(obj.obj.getLocalScale().scale(0.5));
}

laserSplash.prototype.init = function(pos, nor){
    var lsobj = new pc.Entity();
    //lsobj.addComponent("model", {type: "sphere"});
    app.systems.model.addComponent(lsobj, {
        type: "asset",
        asset: getObj('Splash.json', "model")
    });

    lsobj.setLocalScale(0.5, 0.5, 0.5)
    lsobj.rotate(Math.random()*180, Math.random()*180, Math.random()*180);
    lsobj.translate(pos);


    //lsobj.lookAt(playerObj.obj.getPosition(), lsobj.forward)
    //lsobj.lookAt(playerObj.getPosition);

    app.root.addChild(lsobj);

    this.obj = lsobj;
}

laserSplash.prototype.remove = function(dt, lsobj){
    lsobj.obj.destroy();
}

function addLaserSplash(pos, nor){
    //Creates laser splash at the specified object and rotation
    lsobj = new laserSplash();
    lsobj.init(pos, nor);
    addTask(lsobj.remove, 1, 1, lsobj);
    addTask(lsobj.shrink, 0, 10, lsobj);
}
continueloading=true;
