task_list = []


function task(){
    funct = null;
    toRun = 0;
    delay = 0;
    time = 0.0;
    parem = []
    
}

function addTask(funct, delay, toRun, parem){
    tempTask = new task()
    tempTask.funct = funct
    tempTask.delay = delay
    tempTask.toRun = toRun
    tempTask.time = 0.0;
    tempTask.parem = parem
    task_list.push(tempTask)
    return tempTask;
}

function endTask(ta){
    t = task_list.indexOf(ta)
    task_list.splice(t, 1);
}

function updateTasks(dt){
    for (t in task_list){
        ta = task_list[t]
        ta.time += dt;
        if (ta.time > ta.delay ){
            ta.funct(dt, ta.parem);
            ta.time = 0.0;
            if (ta.toRun >= 0){
                ta.toRun -= 1;
                if (ta.toRun == 0){
                    endTask(ta);
                }
            }
        }
    }
}

function resetTaskTime(task){
    //Resets the last_runtime of a task to the current time
    task.time = 0
}

continueloading = true;
