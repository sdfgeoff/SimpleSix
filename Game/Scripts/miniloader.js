//Loads Javascript files without requiring the full loader
REVISION = Math.floor(Math.random()*10000)

function requirejs(url){
	head = document.getElementsByTagName('head')[0]
	script = document.createElement('script')
	script.type = 'text/javascript'
	script.src = '../Game/Scripts/'+url+'?v='+REVISION
	head.appendChild(script)
	head.removeChild(script)
}
